import 'package:angular/angular.dart';
import 'package:angulardart_gitlab/app_component.template.dart' as ng;

void main() {
  runApp(ng.AppComponentNgFactory);
}
